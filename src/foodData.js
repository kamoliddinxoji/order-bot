// src/foodData.js
const foodData = [
    {
      id: 1,
      name: 'Classic Burger',
      category: 'Burgers',
      price: 6.99,
    },
    {
      id: 2,
      name: 'Margherita Pizza',
      category: 'Pizza',
      price: 8.99,
    },
    {
      id: 3,
      name: 'Chicken Alfredo Pasta',
      category: 'Pasta',
      price: 10.99,
    },
    // Add more food items here
  ];
  
  export default foodData;
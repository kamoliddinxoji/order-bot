import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUtensils } from '@fortawesome/free-solid-svg-icons'; // Example FontAwesome icon

import './FoodItem.css';

function FoodItem({ food }) {
  const [count, setCount] = useState(0);

  const handleIncrement = () => {
    setCount(count + 1);
  };

  const handleDecrement = () => {
    if (count > 0) {
      setCount(count - 1);
    }
  };

  return (
    <div className="food-item">
      <FontAwesomeIcon icon={faUtensils} size="2x" /> {/* FontAwesome icon */}
      <h3>{food.name}</h3>
      <p>Category: {food.category}</p>
      <p>Price: ${food.price.toFixed(2)}</p>
      <div className="counter">
        <button onClick={handleDecrement}>-</button>
        <span>{count}</span>
        <button onClick={handleIncrement}>+</button>
      </div>
    </div>
  );
}

export default FoodItem;
const foodData = [
    {
      id: 1,
      name: 'Burger',
      category: 'Fast Food',
      price: 5.99,
    },
    {
      id: 2,
      name: 'Pizza',
      category: 'Fast Food',
      price: 8.99,
    },
    // Add more food items here
  ];
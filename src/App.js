// src/App.js
import React from 'react';
import './App.css';
import FoodItem from './components/FoodItem';
import foodData from './foodData';

function App() {
  return (
    <div className="app">
      <h1>Food Menu</h1>
      <div className="food-list">
        {foodData.map((food) => (
          <FoodItem key={food.id} food={food} />
        ))}
      </div>
    </div>
  );
}

export default App;